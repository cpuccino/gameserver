from fastapi import FastAPI
import uvicorn
from resource_probe import ResourceProbe

app = FastAPI()


@app.get("/stats")
async def get_stats():
    resource_probe = ResourceProbe()

    return {
        "uptime": resource_probe.get_uptime(),
        "battery_drain": f"{round(resource_probe.get_battery_drain(), 2)}W",
        "battery_remaining": f"{round(resource_probe.get_battery_remaining(), 2)}%",
        "cpu_usage": f"{round(resource_probe.get_cpu_usage(), 2)}%",
        "gpu_usage": f"{round(resource_probe.get_gpu_usage(), 2)}%",
        "ram_used": f"{round(resource_probe.get_ram_used(), 2)}GB",
        "ram_total": f"{round(resource_probe.get_ram_total(), 2)}GB",
        "storage_used": f"{round(resource_probe.get_storage_used(), 2)}GB",
        "storage_total": f"{round(resource_probe.get_storage_total(), 2)}GB",
    }


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8003)
