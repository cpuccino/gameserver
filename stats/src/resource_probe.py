import re
import subprocess
import psutil
from typing import Tuple, Optional


class ResourceProbe:
    def get_uptime(self) -> Optional[str]:
        try:
            output = subprocess.check_output(["uptime", "-p"])
            return output.decode().strip()
        except subprocess.CalledProcessError:
            return None

    def get_battery_remaining(self) -> Optional[float]:
        battery = psutil.sensors_battery()
        return battery.percent if battery is not None else None

    def get_battery_drain(self) -> Optional[float]:
        try:
            output = subprocess.check_output(
                ["upower", "-i", "/org/freedesktop/UPower/devices/battery_BAT0"]
            )

            energy_rate_pattern = r"energy-rate:\s*([\d.-]+) W", output.decode("utf-8")
            energy_rate_match = re.search(energy_rate_pattern)

            return (
                float(energy_rate_match.group(1))
                if energy_rate_match is not None
                else None
            )
        except subprocess.CalledProcessError:
            return None

    def get_cpu_usage(self) -> float:
        return psutil.cpu_percent(interval=1)

    def get_ram_used(self) -> float:
        return self.__bytes_to_gigabytes(psutil.virtual_memory().used)

    def get_ram_total(self) -> float:
        return self.__bytes_to_gigabytes(psutil.virtual_memory().total)

    def get_storage_used(self) -> Tuple[float, float]:
        disk_usage = psutil.disk_usage("/")
        return self.__bytes_to_gigabytes(disk_usage.used)

    def get_storage_total(self) -> Tuple[float, float]:
        disk_usage = psutil.disk_usage("/")
        return self.__bytes_to_gigabytes(disk_usage.total)

    def get_gpu_usage(self) -> Optional[float]:
        try:
            output = subprocess.check_output(
                [
                    "nvidia-smi",
                    "--query-gpu=gpu_name,utilization.gpu",
                    "--format=csv,noheader,nounits",
                ]
            )
            gpu_info = output.decode().strip().split(",")
            gpu_usage = float(gpu_info[1])
            return gpu_usage
        except subprocess.CalledProcessError:
            return None

    def __bytes_to_gigabytes(self, bytes: float) -> float:
        bytes_in_gigabytes = 1024 * 1024 * 1024
        return bytes / bytes_in_gigabytes
