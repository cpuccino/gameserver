all:
	make help

# package: Configure wings, panel and stats endpoint for deployment
package:
	@if [ -d build ]; then \
		read -p "Configuration will be overriden. Do you want to proceed? (y/n): " answer; \
		if [ "$$answer" != "y" ]; then echo "Aborting."; exit 1; fi; fi

	@rm -rf build && mkdir build
	@cp -r wings panel stats build/

	@echo "Initializing setup."

	@echo "Configuring panel."; \
	config_file="build/panel/docker-compose.yml"; \
	placeholders=$$(grep -oP '{{\K[^}]+' "$$config_file" | sort -u) ; \
	for placeholder in $$placeholders; do \
		read -p "Enter value for $$placeholder: " replacement; \
		sed -i "s|{{$$placeholder}}|$$replacement|g" $$config_file; \
	done

	@echo "Configuring stats endpoint."; \
		cd build/stats && \
		python3 -m venv venv && \
		( \
			. venv/bin/activate && \
			pip install --upgrade pip && \
			pip install -r requirements.txt \
		)

	@echo "Setup complete."

# help: Display the help page.
help:
	@awk 'BEGIN{FS=":"}{if(/^# [a-z.A-Z_-]+:.*/){printf "%-30s %s\n",substr($$1, 3), $$2 }}' $(MAKEFILE_LIST) | sort
