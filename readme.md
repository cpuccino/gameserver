### Gameserver

### Standard configuration

- Expose panel, wings and util via nginx.
- Letsencrypt certs for panel, wings and util.
- Wings configuration.
  - FQDN (wings domain)
  - Behind Proxy - true
  - Daemon Port - 443
  - Daemon SFTP Port - 2022

### Troubleshooting

- You can view errors via pterodactyl logs.
  - If logs aren't getting generated make sure the panel has the necessary write permissions.
    `docker compose exec panel chown -R nginx: /app/storage/logs/`
- If the wings node already has SSL configured.
  - Then `/etc/pterodactyl`.`api.ssl` should be disabled otherwise we will run into SSL errors.
  - Then "Behind Proxy" should be true. This ensures that the daemon doesn't try to use a certifacte.
